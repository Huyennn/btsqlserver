﻿CREATE TABLE phong_ban(
ma_phongban varchar(7) primary key,
ten_phongban varchar(50)
);

CREATE TABLE nhan_vien(
ma_nhanvien varchar(7) primary key ,
ten_nhanvien varchar(50) ,
ngay_sinh date check(ngay_sinh < getdate()),
so_cmnd char(9), 
gioi_tinh char(1) default('M'),
dia_chi varchar(100),
ngayvaolam datetime, 
ma_phongban varchar(7) foreign key references phong_ban(ma_phongban)
);
--alter table nhan_vien alter column ngayvaolam  add check( getdate() - ngay_sinh >20);

CREATE TABLE luong_da(
ma_da varchar(8) primary key ,
ma_nhanvien varchar(7) foreign key references nhan_vien(ma_nhanvien),
ngay_nhan datetime DEFAULT GETDATE() not null,
so_tien money check (so_tien >0)
);

--1. Thực hiện chèn dữ liệu vào các bảng vừa tạo (ít nhất 5 bản ghi cho mỗi bảng).
insert into phong_ban(ma_phongban, ten_phongban) values ('HR12345', 'Nhan Su');
insert into phong_ban(ma_phongban, ten_phongban) values ('MK12345', 'Phong Marketing ');
insert into phong_ban(ma_phongban, ten_phongban) values ('OP12345', 'Hop Tac');
insert into phong_ban(ma_phongban, ten_phongban) values ('TE12345', 'Ki Thuat');
insert into phong_ban(ma_phongban, ten_phongban) values ('BA12345', 'Dieu Hanh');

insert into nhan_vien(ma_nhanvien,ten_nhanvien,  ngay_sinh, so_cmnd , gioi_tinh , dia_chi, ngayvaolam, ma_phongban)
values ('NV01' , 'Pham Thi Huyen', '05/11/1995', '012345678', 'M',  'Ha Noi', '05/11/2019', 'BA12345');
insert into nhan_vien(ma_nhanvien,ten_nhanvien,  ngay_sinh, so_cmnd , gioi_tinh , dia_chi, ngayvaolam, ma_phongban) 
values ('NV02' , 'Pham Thi Huyen', '05/11/1993', '012345675', 'M', 'Hai Phong', 05/11/2019, 'TE12345');
insert into nhan_vien(ma_nhanvien,ten_nhanvien,  ngay_sinh, so_cmnd , gioi_tinh , dia_chi, ngayvaolam, ma_phongban) 
values ('NV03' , 'Le Thi Hien', '05/11/1996', '012345671', 'F', 'Bac Ninh', 05/11/2019, 'OP12345');
insert into nhan_vien(ma_nhanvien,ten_nhanvien,  ngay_sinh, so_cmnd , gioi_tinh , dia_chi, ngayvaolam, ma_phongban) 
values ('NV04' , 'Nguyen Thuy Linh', '05/11/1990', '012315675', 'M', 'Nam Dinh', 05/11/2019, 'HR12345');
insert into nhan_vien(ma_nhanvien,ten_nhanvien,  ngay_sinh, so_cmnd , gioi_tinh , dia_chi, ngayvaolam, ma_phongban) 
values ('NV05' , 'Pham Anh Quan', '05/11/1991', '112345675', 'F', 'Hai Phong', 05/11/2018, 'MK12345');
insert into nhan_vien(ma_nhanvien,ten_nhanvien,  ngay_sinh, so_cmnd , gioi_tinh , dia_chi, ngayvaolam, ma_phongban) 
values ('NV06' , 'Le Hai Nam', '05/11/1991', '112345605', 'F', 'Hai Phong', 05/11/2018, 'MK12345');

insert into luong_da(ma_da, ma_nhanvien, ngay_nhan, so_tien) values ('da01', 'NV01', '01/6/2019', 50000000)
insert into luong_da(ma_da, ma_nhanvien, ngay_nhan, so_tien) values ('da02', 'NV05', '01/4/2019', 50000000)
insert into luong_da(ma_da, ma_nhanvien, ngay_nhan, so_tien) values ('da03', 'NV04', '01/2/2019', 50000000)
insert into luong_da(ma_da, ma_nhanvien, ngay_nhan, so_tien) values ('da04', 'NV02', '01/1/2019', 50000000)
insert into luong_da(ma_da, ma_nhanvien, ngay_nhan, so_tien) values ('da05', 'NV03', '01/9/2019', 50000000)

--2. Viết một query để hiển thị thông tin về các bảng LUONGDA, NHANVIEN, PHONGBAN.
select * from phong_ban;
select * from nhan_vien;
select * from luong_da;

--3. Viết một query để hiển thị những nhân viên có giới tính là ‘F’.
select * from nhan_vien where gioi_tinh = 'F';

--4. Hiển thị tất cả các dự án, mỗi dự án trên 1 dòng.
select *  from luong_da;

--5. Hiển thị tổng lương của từng nhân viên (dùng mệnh đề GROUP BY).
SELECT ten_nhanvien, ma_da, so_tien
FROM dbo.luong_da INNER JOIN dbo.nhan_vien 
ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien

--6. Hiển thị tất cả các nhân viên trên một phòng ban cho trước (VD: ‘Hành chính’).
select nhan_vien.* , phong_ban.ten_phongban from dbo.nhan_vien INNER JOIN dbo.phong_ban 
ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban
where ten_phongban = 'Nhan Su';

--7. Hiển thị mức lương của những nhân viên phòng nhân sự
select ten_nhanvien, phong_ban.ten_phongban, luong_da.so_tien
from  dbo.luong_da INNER JOIN dbo.nhan_vien 
ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien INNER JOIN dbo.phong_ban 
ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban
where ten_phongban='Nhan Su';

--8. Hiển thị số lượng nhân viên của từng phòng.
select COUNT (ma_nhanvien) as so_nhanvien, ten_phongban  from dbo.nhan_vien INNER JOIN dbo.phong_ban 
ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban 
group by ten_phongban;

--9. Viết một query để hiển thị những nhân viên mà tham gia ít nhất vào một dự án.
select * from dbo.luong_da INNER JOIN dbo.nhan_vien 
ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien 
order by ma_da;
--10. Viết một query hiển thị phòng ban có số lượng nhân viên nhiều nhất.
select count (ma_nhanvien) as 'so_nhanvien', ten_phongban from   dbo.nhan_vien INNER JOIN dbo.phong_ban 
ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban group by ten_phongban having count(ma_nhanvien) >1;
--11. Tính tổng số lượng của các nhân viên trong phòng Hành chính.
select count (ma_nhanvien) from   dbo.nhan_vien INNER JOIN dbo.phong_ban 
ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban where ten_phongban = 'Nhan Su';
--12. Hiển thị tống lương của các nhân viên có số CMND tận cùng bằng 9.
select sum(so_tien) from  dbo.luong_da INNER JOIN dbo.nhan_vien 
ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien 
where so_cmnd ='%5';
--13. Tìm nhân viên có số lương cao nhất.
select top 1* from  dbo.luong_da INNER JOIN dbo.nhan_vien 
ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien order by so_tien desc;
--14. Tìm nhân viên ở phòng nhaan su có giới tính bằng ‘F’ và có mức lương > 1200000.
select nhan_vien.ten_nhanvien, phong_ban.ten_phongban, luong_da.so_tien from
      dbo.luong_da INNER JOIN dbo.nhan_vien 
	  ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien INNER JOIN dbo.phong_ban 
	  ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban where gioi_tinh ='M' and so_tien >5000000 and ten_phongban ='Nhan Su';

--15. Tìm tổng lương trên từng phòng.
select sum(so_tien) from dbo.luong_da CROSS JOIN dbo.phong_ban group by ten_phongban;

--16. Liệt kê các dự án có ít nhất 2 người tham gia.
select ma_da, ten_nhanvien from dbo.luong_da INNER JOIN dbo.nhan_vien 
ON dbo.luong_da.ma_nhanvien = dbo.nhan_vien.ma_nhanvien where  

--17. Liệt kê thông tin chi tiết của nhân viên có tên bắt đầu bằng ký tự ‘N’.
select * from nhan_vien where ten_nhanvien like 'N%';

--18. Hiển thị thông tin chi tiết của nhân viên được nhận tiền dự án trong năm 2003.


--19. Hiển thị thông tin chi tiết của nhân viên không tham gia bất cứ dự án nào.

--20. Xoá dự án có mã dự án là DAD02.
delete from luong_da where ma_da = 'DA02';
--21. Xoá đi từ bảng LuongDA những nhân viên có mức lương 2000000.

--22. Cập nhật lại lương cho những người tham gia dự án DA01 thêm 10% lương cũ.
update luong_da set so_tien = so_tien + so_tien*0.1 where ma_da ='DA01';
--23. Xoá các bản ghi tương ứng từ bảng NhanVien đối với những nhân viên không có mã nhân viên tồn tại trong bảng LuongDA.

--24. Viết một truy vấn đặt lại ngày vào làm của tất cả các nhân viên thuộc phòng hành chính là ngày 12/02/1999.
update nhan_vien set ngayvaolam ='12/02/1999' from  dbo.nhan_vien INNER JOIN dbo.phong_ban 
ON dbo.nhan_vien.ma_phongban = dbo.phong_ban.ma_phongban 
where ten_phongban ='Nhan Su';


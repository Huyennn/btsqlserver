
--cau2
USE AdventureWorks2012
SELECT*INTO LAB10_index.dbo.WorkOrder FROM Production.WorkOrder
--cau3
USE LAB10_index
SELECT*INTO WorkOrderIX FROM WorkOrder
--cau4
SELECT*FROM WorkOrder
SELECT*FROM WorkOrderIX

--cau6
CREATE INDEX IX_WorkOrderID ON WorkOrderIX(WorkOrderID)

--cau7
SELECT*FROM WorkOrder where WorkOrderID=72000


CREATE TABLE nha_xuat_ban(
ma_nhaxuatban int NOT NULL IDENTITY(1,1) PRIMARY KEY,
ten_nhaxuatban varchar(255) NOT NULL,
diachi_nhaxuatban varchar(255) NOT NULL
);

CREATE TABLE the_loai(
ma_theloai int NOT NULL IDENTITY(1,1) PRIMARY KEY,
ten_theloai varchar(50)  

);
CREATE TABLE sach(
ma_sach int NOT NULL IDENTITY(1,1) PRIMARY KEY,
ten_sach varchar(50) NOT NULL,
tac_gia varchar(50) NOT NULL, 
noi_dung varchar(255),
nam_xuatban date NOT NULL,
lan_xuatban int NOT NULL,
gia_ban int NOT NULL,
soluong int,
ma_nhaxuatban int FOREIGN KEY references nha_xuat_ban(ma_nhaxuatban),
ma_theloai int FOREIGN KEY references the_loai(ma_theloai)
);
DROP TABLE nha_xuat_ban;
DROP TABLE the_loai;
DROP TABLE sach;
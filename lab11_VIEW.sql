USE lab11_view
go
--2
CREATE VIEW ProductList
AS	
SELECT ProductID, Name FROM AdventureWorks2012.Production.Product

--3
SELECT * FROM ProductList
--bai2
--1
CREATE VIEW SalesOrderDetail
AS
SELECT pr.ProductID, pr.Name, od.UnitPrice, od.OrderQty,
od.UnitPrice*od.OrderQty as [Total Price]
FROM AdventureWorks2012.Sales.SalesOrderDetail od
JOIN AdventureWorks2012.Production.Product pr
ON od.ProductID=pr.ProductID
--2
select * FROM SalesOrderDetail


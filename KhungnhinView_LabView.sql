﻿use aptech_view_booksold
go
create table Customer(
CustomerID int identity primary key,
CustomerName varchar(50),
Address varchar(100),
Phone varchar (12)
);

create table Book(
BookCode int identity primary key,
Category varchar(50),
Author varchar(50),
Publisher varchar(50),
Title varchar(100),
Price  int,
InStore int
);

create table BookSold(
BookSoldID int identity primary key,
CustomerID int foreign key references Customer(CustomerID),
BookCode int foreign key references Book(BookCode),
Date date,
Price int,
Amount int 
);
--1. Chèn ít nhất 5 bản ghi vào bảng Books, 5 bản ghi vào bảng Customer và 10 bản ghi vào bảng
BookSold.
insert into Customer(CustomerName, Address,Phone) 
values('Le Dinh Hai', 'Ha Noi', 03770970073);
insert into Customer(CustomerName, Address,Phone) 
values('Luong Dai', 'Lang Son', 0123456780);
insert into Customer(CustomerName, Address,Phone) 
values('Thien Phu', 'Hung Yen', 0978546211);
insert into Customer(CustomerName, Address,Phone) 
values('Ha Bac', 'Ha Noi', 01587464445);
insert into Customer(CustomerName, Address,Phone) 
values('Huyen Pham', 'Hai Duong', 0377097444);
select * from Customer

insert into Book( Category, Author,Publisher, Title, Price, InStore)
values ('Van hoc', 'To Hoai', 'Kim Dong', 'De men phieu luu ki', 50000, 30)

insert into Book( Category, Author,Publisher, Title, Price, InStore)
values('Kinh te', 'Shark Hung', 'Giao duc', 'Cach lam giau', 100000, 5)

insert into Book( Category, Author,Publisher, Title, Price, InStore)
values ('Tin hoc;', 'To Huu', 'Su pham', 'Nho Viet Bac', 400000, 100)

insert into Book( Category, Author,Publisher, Title, Price, InStore)
values ('Toan hoc', 'Ba huyen thanh quan', 'Tuoi Tre', 'Qua deo ngang', 100000, 25)

insert into Book( Category, Author,Publisher, Title, Price, InStore)
values ('Chuyen co tich', 'Nguyen Tuan', 'Tuoi Tre', 'Tam Cam', 100000, 25)
select * from Book

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (2, 1, '05/05/2000', 150000, 23)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (3, 2, '05/05/2001', 150000, 12)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (4, 3, '05/05/2002', 150000, 22)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (5, 4, '05/05/2003', 150000, 1)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (1, 5, '05/05/2004', 150000, 20)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (4, 3, '05/05/2004', 150000, 13)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (3, 4, '05/05/2005', 150000, 23)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (5, 2, '05/05/2006', 150000, 6)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (2, 5, '05/05/2007', 150000, 7)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (5, 1, '05/05/2008', 200000, 10)
select * from BookSold

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (4, 1, '05/10/2019', 200000, 10)


insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (3, 1, '05/08/2019', 200000, 20)

insert into BookSold(CustomerID, BookCode, Date,Price, Amount)
values (2, 1, '10/08/2019', 200000, 15)


--2. Tạo một khung nhìn chứa danh sách các cuốn sách (BookCode, Title, Price) kèm theo số lượng đã
--bán được của mỗi cuốn sách.
--Gợi ý: Trường Amout của bảng BookSold chứa số lượng sách đã bán cho một khách hàng.
Create view SoLuongSachDaBan
as
select distinct Book.BookCode, Book.Title, Book.Price, BookSold.Amount from Book inner join BookSold
on Book.BookCode = BookSold.BookCode

select * from SoLuongSachDaBan

--3. Tạo một khung nhìn chứa danh sách các khách hàng (CustomerID, CustomerName, Address) kèm
--theo số lượng các cuốn sách mà khách hàng đó đã mua.
CREATE VIEW DanhSachKhachHang
as
select Customer.CustomerID, Customer.CustomerName, Customer.Address, BookSold.Amount
from dbo.BookSold INNER JOIN dbo.Customer 
ON dbo.BookSold.CustomerID = dbo.Customer.CustomerID

select * from DanhSachKhachHang

--4. Tạo một khung nhìn chứa danh sách các khách hàng (CustomerID, CustomerName, Address) đã
--mua sách vào tháng trước, kèm theo tên các cuốn sách mà khách hàng đã mua.
CREATE VIEW DSKH_muaSachThangTruoc
as
select Customer.CustomerID, Customer.CustomerName, Customer.Address, BookSold.Date, Book.Title 
from  dbo.Book INNER JOIN dbo.BookSold 
ON dbo.Book.BookCode = dbo.BookSold.BookCode INNER JOIN dbo.Customer 
ON dbo.BookSold.CustomerID = dbo.Customer.CustomerID
where YEAR(date) =2019 and month(date) = 10

select * from DSKH_muaSachThangTruoc


--5. Tạo một khung nhìn chứa danh sách các khách hàng kèm theo tổng tiền mà mỗi khách hàng đã chi
--cho việc mua sách.
--Gợi ý: Tiền mỗi lần mua = Giá mỗi cuốn sách * Số lượng sách đã mua.
CREATE VIEW  TongTien
as
select Customer.*, BookSold.Amount, BookSold.Price, BookSold.Amount * BookSold.Price as [ToTal_Price]
from  dbo.BookSold INNER JOIN dbo.Customer 
ON dbo.BookSold.CustomerID = dbo.Customer.CustomerID


drop view TongTien

select * from TongTien
﻿use BTVN_khungnhinView_View
go

create table Class(
ClassCode varchar(10) primary key,
HeadTeacher varchar(30),
Room varchar(30),
TimeSlot char,
CloseDate datetime
);

create table Student(
RollNo varchar(10) primary key,
ClassCode varchar(10) foreign key references Class(ClassCode),
FullName varchar(30),
Male bit,
BirthDate datetime,
Address varchar(30),
Provice char(2),
Email varchar(30)
);
drop table Student
drop table Subject
Drop table Mark


create table Subject(
SubjectCode varchar(10) primary key,
SubjectName varchar(40),
WMark bit,
PMark bit,
WTest_per int,
PTest_per int
);

Create table Mark (
RollNo varchar(10) foreign key references Student(RollNo),
SubjectCode varchar(10) foreign key references Subject(SubjectCode),
WMark float,
PMark float,
Mark float
);


--1. Chèn ít nhất 5 bản ghi cho từng bảng ở trên.
insert into Class(ClassCode, HeadTeacher,Room, TimeSlot, CloseDate)
values('C1007L', 'Quang Hoa', 'Class 3', 'G', '05/11/2009')

insert into Class(ClassCode, HeadTeacher,Room, TimeSlot, CloseDate)
values('C2007L', 'Hong Luyen', 'Class 4', 'M', '05/12/2009')

insert into Class(ClassCode, HeadTeacher,Room, TimeSlot, CloseDate)
values('C3007L', 'Nguyen Tuan', 'Class 5', 'E', '1/11/2009')

insert into Class(ClassCode, HeadTeacher,Room, TimeSlot, CloseDate)
values('C4007L', 'Quang Hoa', 'Class 6', 'A', '10/11/2009')

insert into Class(ClassCode, HeadTeacher,Room, TimeSlot, CloseDate)
values('C5007L', 'Quang Hoa', 'Class 7', 'M', '6/11/2009')
select * from Class
----------------------------------------------------------------
Insert into Student(RollNo, ClassCode, FullName, Male, BirthDate, Address, Provice, Email)
values ('A00264', 'C5007L', 'Pham Thi Huyen', 0, '05/11/1998', 'Hoang Mai', 'HD', 'Phamhuyen123@gmail.com')


Insert into Student(RollNo, ClassCode, FullName, Male, BirthDate, Address, Provice, Email)
values ('B00264', 'C4007L', 'Le Hien', 1, '05/1/1998', 'Hai Ba Trung', 'HN', 'Lehien123@gmail.com')


Insert into Student(RollNo, ClassCode, FullName, Male, BirthDate, Address, Provice, Email)
values ('C00264', 'C3007L', 'Thu Trang', 1, '05/3/1997', 'Hoang Mai', 'BN', 'Thutrang123@gmail.com')


Insert into Student(RollNo, ClassCode, FullName, Male, BirthDate, Address, Provice, Email)
values ('D00264', 'C2007L', 'Luong Dai', 0, '05/9/1996', 'Hoang Mai', 'LS', 'Luongdai123@gmail.com')


Insert into Student(RollNo, ClassCode, FullName, Male, BirthDate, Address, Provice, Email)
values ('E00264', 'C1007L', 'Thien Phu', 0, '05/10/1995', 'Hoang Mai', 'HY', 'ThienPhu123@gmail.com')

select * from Student 

--Subject----------------------------------------------------------------
insert into Subject(SubjectCode, SubjectName, WMark, PMark, WTest_per, PTest_per)
values ('EPC', 'Elementary Programing with C',1,1,8,5)

insert into Subject(SubjectCode, SubjectName, WMark, PMark, WTest_per, PTest_per)
values ('JV', 'Java',1,1,4,4)

insert into Subject(SubjectCode, SubjectName, WMark, PMark, WTest_per, PTest_per)
values ('C++', 'Lap trinh',1,1,7,8)

insert into Subject(SubjectCode, SubjectName, WMark, PMark, WTest_per, PTest_per)
values ('HTML', 'Ngon ngu',1,1,5,4)

insert into Subject(SubjectCode, SubjectName, WMark, PMark, WTest_per, PTest_per)
values ('CSS', 'Thiet ke',1,1,6,6)
select * from Subject
----------------------------------------------------------------
insert into Mark(RollNo, SubjectCode, WMark, PMark, Mark)
values ('A00264', 'JV', 8.8, 5, 7 )

insert into Mark(RollNo, SubjectCode, WMark, PMark, Mark)
values ('B00264', 'C++', 8.8, 5, 7 )

insert into Mark(RollNo, SubjectCode, WMark, PMark, Mark)
values ('B00264', 'C++', 5, 5, 8 )

insert into Mark(RollNo, SubjectCode, WMark, PMark, Mark)
values ('C00264', 'HTML', 3, 5, 6 )

insert into Mark(RollNo, SubjectCode, WMark, PMark, Mark)
values ('D00264', 'CSS', 10, 5, 4)

insert into Mark(RollNo, SubjectCode, WMark, PMark, Mark)
values ('E00264', 'JV', 6, 5, 5 )
select * from Mark



--2. Tạo một khung nhìn chứa danh sách các sinh viên đã có ít nhất 2 bài thi (2 môn học khác nhau).
create view DS_Sinhvien
as
select Mark.RollNo
from dbo.Mark INNER JOIN dbo.Student 
ON dbo.Mark.RollNo = dbo.Student.RollNo
group by Mark.RollNo
having COUNT(Mark.RollNo) >=2

drop view DS_Sinhvien
select * from DS_Sinhvien

--3. Tạo một khung nhìn chứa danh sách tất cả các sinh viên đã bị trượt ít nhất là một môn.
create view DS_SV_truot
as 
select Student.RollNo, Student.FullName , Subject.SubjectCode,  Mark.Mark
from dbo.Mark INNER JOIN dbo.Student 
ON dbo.Mark.RollNo = dbo.Student.RollNo INNER JOIN dbo.Subject 
ON dbo.Mark.SubjectCode = dbo.Subject.SubjectCode
where Mark <5
select * from DS_SV_truot
--4. Tạo một khung nhìn chứa danh sách các sinh viên đang học ở TimeSlot G.
create view DS_TimeSlot
as 
select Student.RollNo, Student.FullName, Class.ClassCode, Class.CloseDate, Class.TimeSlot
from   dbo.Class INNER JOIN dbo.Student 
ON dbo.Class.ClassCode = dbo.Student.ClassCode
where TimeSlot ='G'
drop view DS_TimeSlot
select * from DS_TimeSlot
--5. Tạo một khung nhìn chứa danh sách các giáo viên có ít nhất 3 học sinh thi trượt ở bất cứ môn nào.
create view DS_GV
as
select HeadTeacher from Class where ClassCode=(Select Student.ClassCode
from Mark inner join Student 
on Student.RollNo =Mark.RollNo inner join Class On Class.ClassCode =Student.ClassCode
where  Mark <5 
group by Student.ClassCode 
having count(Mark.RollNo) >3)

select * from DS_GV
--6. Tạo một khung nhìn chứa danh sách các sinh viên thi trượt môn EPC của từng lớp. Khung nhìn
--này phải chứa các cột: Tên sinh viên, Tên lớp, Tên Giáo viên, Điểm thi môn EPC.
create view DS_SV_GV
as
Select Student.FullName, Class.ClassCode, Class.HeadTeacher, Mark.Mark
from  dbo.Class INNER JOIN dbo.Student 
ON dbo.Class.ClassCode = dbo.Student.ClassCode INNER JOIN dbo.Mark 
ON dbo.Student.RollNo = dbo.Mark.RollNo
where Mark <5 and SubjectCode ='CSS'

select * from DS_SV_GV




